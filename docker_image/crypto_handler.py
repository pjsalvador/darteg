from Cryptodome.Cipher import AES
from Cryptodome.Random import get_random_bytes
import rsa
import json

"""
Returns a random 32 byte string
"""
def getAESKey():
	return get_random_bytes(32)

"""
Receives a 16 byte string that's used to create as AES cipher and encrypts the data.
"""
def encryptBlockAES(AESKey,data):
	cipher = AES.new(AESKey,AES.MODE_EAX)
	try:
		ciphertext, tag = cipher.encrypt_and_digest(data)
		return (ciphertext,tag,cipher.nonce)
	except:
		return

"""
Receives a 16 byte string that's used to create an AES cipher and decrypts the data.
"""
def decryptBlockAES(AESKey,ciphertext,nonce,tag):
	try:
		cipher = AES.new(AESKey,AES.MODE_EAX,nonce)
		data = cipher.decrypt_and_verify(ciphertext,tag)
	except:
		return
	return data

"""
Signs data with a given RSA private key.
"""
def signData(privKey,data):
	return rsa.sign(data,privKey,'SHA-256')


"""
Verifies the signature with a public key.
"""
def verifySig(pubKey,signature,data):
	return rsa.verify(data,signature,pubKey)

"""
Receives a private RSA key and encrypted data, returns the data decrypted.
"""
def decryptBlockRSA(privKey,data):
	try:
		return rsa.decrypt(data,privKey)
	except:
		return
"""
Receives a public RSA key and encrypts data.
"""
def encryptBlockRSA(pubKey,data):
	try:
		return rsa.encrypt(data,pubKey)
	except:
		return
"""
Encrypts data with a random AES key.
Signs the encrypted data, and encrypts the AES key with RSA
"""
def encryptAndSign(dstPubKey,srcPrivKey,data):
	try:
		tmp_aes_key = getAESKey()
		(ciphertext,tag,nonce) = encryptBlockAES(tmp_aes_key,data)
		encrypted_aes_key = encryptBlockRSA(dstPubKey,tmp_aes_key)
		tmp_aes_key = None
		signature = signData(srcPrivKey,ciphertext)
		return signature,encrypted_aes_key,ciphertext,tag,nonce
	except:
		return

"""
Decrypts AES key, verifies Signature and decrypt data
"""
def decryptAndVerify(pubKey,privKey,signature,encrypted_aes_key,ciphertext,tag,nonce):
	if not verifySig(pubKey,signature,ciphertext):
		return
	decrypted_aes_key = decryptBlockRSA(privKey,encrypted_aes_key)
	data = decryptBlockAES(decrypted_aes_key,ciphertext,nonce,tag)
	return data
