import os
import sys
from generate_key_pair import generateKeys
def checkIfKeysExist():
	if not os.path.exists('shared/RSA/key.priv'):
		generateKeys(path='shared/RSA')
		sys.exit(0)
	else:
		sys.exit(1)
		pass

if __name__ == '__main__':
	checkIfKeysExist()
