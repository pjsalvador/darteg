import re
import subprocess
import psutil
def getDiskUsage():
	try:
		output = subprocess.check_output('df -h',shell=True,universal_newlines=True)
		count = 0
		pos = 0
		for line in output.split('\n'):
			new_line = re.sub('\s+', ' ',line).strip()
			new_line = new_line.split(' ')
			for nl in new_line:
				count+=1
				if nl == 'Use%':
					print(pos)
					pos = count
				elif nl == '/':
					return re.sub('%','',new_line[pos-1])
			count = 0
		return -1
	except:
		return -1

def getCpuUsage():
	try:
		return psutil.cpu_percent(interval=1)
	except:
		return -1

def main():
	tmp = dict()
	tmp["CPU"] = getCpuUsage()
	tmp["DISK"] = getDiskUsage()
	print(tmp)
	return tmp

if __name__ == '__main__':
	main()