from flask import Flask, request
import json
import threading
import atexit
import subprocess
import os
import random
import string
from IPy import IP
import sys
import rsa
import base64
import crypto_handler
import cpuDiskStats
import urllib3
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.interval import IntervalTrigger
app = Flask(__name__)
private_key = None
mainframe_key = None
configs = json.loads(open('flask_configs.txt','r').read())
probeID = ""
server = ""
TIMER = 120
urllib3.disable_warnings()
cron = BackgroundScheduler()
cron.start()

def keepAlive():
    print("KEEP ALIVE")
    tmp = dict()
    data = dict()
    data["CPU"] = cpuDiskStats.getCpuUsage()
    data["DISK"] = cpuDiskStats.getDiskUsage()
    tmp["ID"] = probeID
    try:
        signature,encrypted_aes_key,ciphertext,tag,nonce = crypto_handler.encryptAndSign(mainframe_key,private_key,json.dumps(data).encode())
    except:
        return json.dumps(dict())
    tmp["Nonce"] = base64.b64encode(nonce).decode("utf-8")
    tmp["Signature"] = base64.b64encode(signature).decode("utf-8")
    tmp["Data"] = base64.b64encode(ciphertext).decode("utf-8")
    tmp["Tag"] = base64.b64encode(tag).decode("utf-8")
    tmp["AESKey"] = base64.b64encode(encrypted_aes_key).decode("utf-8")
    enc_body = json.dumps(tmp)
    http = urllib3.PoolManager()
    req = http.request('POST',server+"/keepAlive",headers={'Content-Type':'application/json'},body=enc_body)
    print("request sent")

cron.add_job(func=keepAlive, trigger=IntervalTrigger(seconds=TIMER), id='keepAlive', replace_existing=True)
atexit.register(lambda: cron.shutdown())
@app.route("/")
def home():
	return Response("Nothing to see here")

@app.route("/receiveData",methods=['POST'])
def receiveData():
	data = request.get_json()
	sendToMainframe(data)
	return json.dumps({"Success":1}),200

"""
Encrypts and sends data to the mainframe
"""

def sendToMainframe(data):
	tmp = dict()
	tmp["ID"] = probeID
	try:
		signature,encrypted_aes_key,ciphertext,tag,nonce = crypto_handler.encryptAndSign(mainframe_key,private_key,json.dumps(data).encode())
	except:
		return json.dumps(dict())
	tmp["Nonce"] = base64.b64encode(nonce).decode("utf-8")
	tmp["Signature"] = base64.b64encode(signature).decode("utf-8")
	tmp["Data"] = base64.b64encode(ciphertext).decode("utf-8")
	tmp["Tag"] = base64.b64encode(tag).decode("utf-8")
	tmp["AESKey"] = base64.b64encode(encrypted_aes_key).decode("utf-8")
	enc_body = json.dumps(tmp)
	http = urllib3.PoolManager()
	req = http.request('POST',server+"/receiveData",headers={'Content-Type':'application/json'},body=enc_body)


"""
Loads RSA public client's key.
"""

def loadRSAKeys():
	print("Loading RSA keys")
	global private_key,mainframe_key
	try:
		with open("shared/RSA/mainframe.pub","r") as pub:
			mainframe_key = rsa.PublicKey.load_pkcs1(pub.read())
		with open("shared/RSA/key.priv","r") as priv:
			private_key = rsa.PrivateKey.load_pkcs1(priv.read())
		return True
	except:
		print("error loading")
		return False

"""
Check if IP address of sender is private.
"""
def checkIPPrivate(ipaddr):
	return IP(ipaddr).iptype() == 'PRIVATE'


def threadFunction(moduleName):
	try:
		print(moduleName)
		subprocess.check_output("python " + os.getcwd() + "/shared/Modules/"+moduleName, shell=True)
	except subprocess.CalledProcessError:
		print("Failure to initialize module")

def startModules():
	print("Starting modules...")
	threadList = []
	try:
		for module in getModules():
			threadList.append(threading.Thread(target=threadFunction,args=(module,)))
			threadList[-1].daemon = True
			threadList[-1].start()
	except Exception as e:
		print(e)
		pass

def getModules():
	print("Getting modules...")
	subdirs = [subdir for subdir in os.listdir(os.getcwd()+'/shared/Modules/') if os.path.isdir(os.getcwd()+'/shared/Modules/'+subdir)]
	files = os.listdir(os.getcwd()+"/shared/Modules/")
	tmp = []
	for subdir in subdirs:
		files = os.listdir(os.getcwd()+"/shared/Modules/"+subdir)
		for f in files:
			if str(f).endswith('.py'):
				tmp.append(subdir+'/'+str(f))
	return tmp

def loadProbeConfigs():
	global probeID,server
	try:
		with open(os.getcwd()+'/shared/probeID.txt','r') as f:
			probeID = json.loads(f.read())['ID']
		with open(os.getcwd()+'/shared/mainframe.json','r') as f:
			config = json.loads(f.read())
			server = 'https://'+config['IP']+':' + str(config['Port'])
	except Exception as e:
		print(e)
		return False
	return True



"""
Launches the server, launching as well the enabled modules.
"""
if __name__ == "__main__":
	if not loadRSAKeys():
		print("Couldn't load RSA keys...Aborting")
	else:
		if not loadProbeConfigs():
			print("Couldn't load probe configs")
		else:
			print("Configs loaded")
			startModules()
			app.run(host=configs["Host"],port=configs["Port"])
