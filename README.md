REQUIREMENTS:
Python3, Python3-env, Pip3, mongoDB

INSTALLATION:
Run the script in the folder "platform_v1_0" using Python 3.

RUNNING:
Edit the fields on the files 'mainframe.json' and 'flask_configs.txt' both on the mainframe folder.
Run the python file 'mainframe.py' using the virtual env created on the same folder (mainframe). To access the website, go to http(s)://host:ip/adminControl.
To create an admin user for the first login, use the script under 'user_scripts' folder. THIS SCRIPT ALWAYS CREATES AN ADMIN USER WITH FULL PRIVILEGES.

UPLOADING MODULES
=== DATA GATHERING ===
Zip file containing the script. The main python file MUST be in the root folder as well as the configuration file (used by the module to read the parameters). The remaining codes can be structured in subfolders. If there is a need to install requirements, the 'requirements.txt' file MUST be on the root folder and it must obey PIP's format.
A JSON file with the required fields MUST be uploaded separately. An example of such file can be seen on the modules folder, under data.

=== DATA ANALYSIS ===
Zip file containing the script. Same requirements as the data gathering modules, without the need for a JSON file or configuration file. Examples can be found under the modules folder.

=== Countermeasures ===
ZIP file(s) with the scripts to activate and optionally a script to deactivate the changes. Examples can also be found under the modules folder.
