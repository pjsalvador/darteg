import subprocess
import os
import zipfile
python_path = os.path.dirname(os.path.abspath(__file__))
def main():
    with zipfile.ZipFile(python_path+'/mainframe.zip') as zipp:
            zipp.extractall(python_path+'/mainframe/')
    cmd = ["python3","-m","venv",python_path+"/mainframe/venv"]
    subprocess.check_output(cmd)
    cmd = python_path+"/mainframe/venv/bin/pip install rsa;"+python_path+"/mainframe/venv/bin/pip install -r mainframe/requirements.txt;source "+python_path+"/mainframe/venv/bin/activate;python RSAgen/generate_key_pair.py " + python_path+"/mainframe/RSAKey/"
    subprocess.check_output(cmd,shell=True)
if __name__ == '__main__':
    main()
