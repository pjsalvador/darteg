import os
from mongoengine import *
import json
import sys
import bcrypt
python_path = os.path.dirname(os.path.abspath(__file__))
sys.path.append(python_path+"/../")

from Models import *
def main():
    fileName = python_path+'/../flask_configs.txt'
    try:
            configs = open(fileName,'r')
            confs = json.loads(configs.read())
    except Exception as e:
        print("Couldn't access flask_configs.txt file.\nError: " +str(e))
    try:
            connect(
                db=confs['Database'],
                username=confs['dbuser'],
                password=confs['dbpass'],
                host=confs['dbhost']
                )
    except Exception as e:
        print("Couldn't connect to mongoDB.\nError: " +str(e))
    print("Welcome to user manager. Please select an option:\n1 - Add user\n2 - Remove user\n3 - List users\n0 - Exit")
    flag = True
    while flag:
        value = input("Option: ")
        try:
            value = int(value)
        except:
            print("Please select a number.")
        if value != 1 and value != 2 and value != 0 and value != 3:
            print("Invalid value.")
        if value == 0:
            return
        elif value == 1:
            user = input("Username:")
            email = input("Email:")
            password = input("Password:")
            if user == '' or email == '' or password == '':
                print("Fields invalid")
            else:
                rt = addUser(user,email,password)
                if rt == 1:
                    print("User added.")
                elif rt == -1:
                    print("User with same email or username exists.")
                else:
                    print("Couldn't add user")
        elif value == 2:
            email = input("Email:")
            if email == '':
                print("Invalid input")
            rt = delUser(email)
            if rt == 1:
                print("User deleted")
            else:
                print("Couldn't delete user")
        else:
            print(userList())



def addUser(username,email,password):
    try:
        emailRep = Users.objects(Email=email).first()
        if emailRep != None:
            return -1
        Users(Username=username,Email=email,Password=bcrypt.hashpw(password.encode(),bcrypt.gensalt(14))).save()
        return 1
    except Exception as e:
        print(e)
        return 0

def userList():
    rtStr = ""
    for user in Users.objects:
        try:
            rtStr += user.Username + " - " + user.Email + "\n"
        except:
            rtStr += "NOTDEF - " +user.Email + "\n"
    if rtStr == "":
        return "None"
    return rtStr

def delUser(email):
    try:
        user = Users.objects(Email=email).first()
        print(user)
        if user:
            user.delete()
            return 1
        return 0
    except:
        return 0
if __name__ == '__main__':
    main()
