import rsa
import os
import sys
def generateKeys(keySize=4096, keyName='key', path=os.getcwd()):
	pub = open(path+'/'+keyName+'.pub','wb')
	priv = open(path+'/'+keyName+'.priv','wb')
	print ("Generating keys...please hold...")
	(pubkey, privkey) = rsa.newkeys(keySize,poolsize=2)
	print ("Key generation done. Saving to file...")
	pub.write(pubkey.save_pkcs1())
	priv.write(privkey.save_pkcs1())
	pub.close()
	priv.close()
	print ("Process complete!!!")

def main():
    path = str(sys.argv[1])
    generateKeys(4096,'key',path=path)
if __name__ == '__main__':
	main()
