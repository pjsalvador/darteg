import argparse
import csv
import subprocess
threshold = 0.15
maxOccur = 3
delta = 300
minCount = 3

def getAvg(rtts):
    averages = []
    count = 0
    for rtt in rtts:
        tmp = rtt.replace(" ","").split(',')
        for t in tmp:
            if len(averages) != len(tmp):
                averages.append(float(t))
            else:
                averages[count] += float(t)
                count+=1
        count = 0
    count = 0
    for avg in averages:
        averages[count] = avg/len(rtts)

    return averages


def getAverage(fileName,startTime,keys,path):
    with open(fileName,'r') as openfile:
        filein = csv.reader(openfile,delimiter='\t')
        avgElemsNoAnom = []
        avgElemsAll = []
        avg = []
        for row in filein:
            try:
                if float(row[keys['HopRTTMonitor.Timestamp']]) >= startTime-delta and float(row[keys['HopRTTMonitor.Timestamp']]) < startTime:
                    if str(row[keys['HopRTTMonitor.Anom']]) == 'False':
                        if path in str(row[keys['HopRTTMonitor.ASNs']]):
                            avgElemsNoAnom.append(str(row[keys['HopRTTMonitor.RTT']]))
                            avgElemsAll.append(str(row[keys['HopRTTMonitor.RTT']]))
                else:
                    if float(row[keys['HopRTTMonitor.Timestamp']]) < startTime-delta:
                        if len(avgElemsNoAnom) < minCount:
                            avgElemsNoAnom = []
                            startTime = startTime-delta
                            continue
                        else:
                            return getAvg(avgElemsNoAnom)
            except Exception as e:
                pass

        if len(avgElemsNoAnom) >= minCount:
            return getAvg(avgElemsNoAnom)

        if len(avgElemsAll) == 0:
            return []

        return getAvg(avgElemsAll)

def getPaths(fileName,startTime,keys):
    with open(fileName,'r') as openfile:
        filein = csv.reader(openfile,delimiter='\t')
        paths = []
        for row in filein:
            try:
                if float(row[keys['HopRTTMonitor.Timestamp']]) >= startTime-delta and float(row[keys['HopRTTMonitor.Timestamp']]) < startTime:
                    if str(row[keys['HopRTTMonitor.Anom']]) == 'False':
                        tmp = str(row[keys['HopRTTMonitor.ASNs']]).strip()
                        if tmp not in paths:
                            paths.append(tmp)
                else:
                    if float(row[keys['HopRTTMonitor.Timestamp']]) < startTime-delta:
                        if len(paths) != 0:
                            return paths
                        else:
                            startTime = startTime-delta
                            continue
            except:
                pass
        return paths


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--file", help="TSV file",type=str,required=True)
    parser.add_argument("-t","--threshold",help="Treshold",type=float)
    parser.add_argument("-m","--maxOccur",help="Max occurences in a row",type=int)
    parser.add_argument("-d","--delta",help="Temporal interval to segment the timeframe (seconds)",type=float)
    parser.add_argument("-c","--count",help="Minimum number of entries to calculate averages")

    args = parser.parse_args()
    if args.threshold:
        global threshold
        threshold = args.threshold
    if args.maxOccur:
        global maxOccur
        maxOccur = args.maxOccur
    if args.delta:
        global delta
        delta = args.delta
    if args.count:
        global minCount
        minCount = args.count

    pathAvg = dict()
    keys = None
    currentPaths = []
    paths = []
    count = 0
    with open(args.file,'r') as filein:
        filein = csv.reader(filein,delimiter='\t')
        for row in filein:
            if keys == None:
                keys = dict()
                for val in row:
                    keys[val] = count
                    count+=1
            else:
                if startTime == 0:
                    startTime = float(row[keys['HopRTTMonitor.Timestamp']])
                    endtime = startTime-delta
                    paths = getPaths(args.file,endtime,keys)
                if float(row[keys['HopRTTMonitor.Timestamp']]) < endtime:
                    break
                else:
                    if str(row[keys['HopRTTMonitor.ASNs']]).strip() not in pathAvg.keys():
                        if str(row[keys['HopRTTMonitor.ASNs']]).strip() in paths:
                            avgPathTmp = getAverage(args.file,endtime,keys,str(row[keys['HopRTTMonitor.ASNs']]).strip())
                            pathAvg[str(row[keys['HopRTTMonitor.ASNs']]).strip()] = avgPathTmp
                        tmp = (str(row[keys['HopRTTMonitor.ASNs']]).strip(),row[keys['HopRTTMonitor.RTT']],float(row[keys['HopRTTMonitor.Timestamp']]))
                        currentPaths.append(tmp)
                    else:
                        tmp = (str(row[keys['HopRTTMonitor.ASNs']]).strip(),row[keys['HopRTTMonitor.RTT']],float(row[keys['HopRTTMonitor.Timestamp']]))
                        currentPaths.append(tmp)
    anomOccur = 0
    anomStart = 0
    flagged = False
    bothUnflaged = True
    lastReadTimeStamp = 0
    for path in currentPaths:
        asn, rtt, time = path[0],path[1], path[2]
        tmp = rtt.replace(" ","").split(',')
        if asn not in paths:
            anomOccur+=1
            bothUnflaged = False
            if anomOccur >= maxOccur:
                if not flagged:
                    flagged = True
                    anomStart = time
                else:
                    lastReadTimeStamp = time
        else:
            if pathAvg[asn] != []:
                c = 0
                for r in rtt:
                    if r > pathAvg[asn][c] * (1+threshold):
                        anomOccur+=1
                        bothUnflaged = False
                        if anomOccur >= maxOccur:
                            if not flagged:
                                flagged = True
                                anomStart = time
                            else:
                                lastReadTimeStamp = time
                        break
                    c+=1
        if bothUnflaged:
            anomOccur = 0
            if flagged:
                flagged = False
        bothUnflaged = True

    return {'Anomaly':flagged,'Start':anomStart,'LastStamp':lastReadTimeStamp}


if __name__ == '__main__':
    print(main())
