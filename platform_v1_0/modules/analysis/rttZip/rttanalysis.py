import argparse
import csv
import subprocess
threshold = 0.15
maxOccur = 3
delta = 1200
minCount = 3

def getAverage(fileName,startTime,keys):
    with open(fileName,'r') as openfile:
        filein = csv.reader(openfile,delimiter='\t')
        avgElemsNoAnom = []
        avgElemsAll = []
        avg = -1
        for row in filein:
            try:
                if float(row[keys['RTTMonitor.Timestamp']]) >= startTime-delta and float(row[keys['RTTMonitor.Timestamp']]) < startTime:
                    if str(row[keys['RTTMonitor.Anom']]) == 'False':
                        avgElemsNoAnom.append(float(row[keys['RTTMonitor.Min']]))
                        avgElemsAll.append(float(row[keys['RTTMonitor.Min']]))
                    else:
                        avgElemsAll.append(float(row[keys['RTTMonitor.Min']]))
                else:
                    if float(row[keys['RTTMonitor.Timestamp']]) < startTime-delta:
                        if len(avgElemsNoAnom) < minCount:
                            avgElemsNoAnom = []
                            startTime = startTime - delta
                            continue
                        else:
                            return sum(avgElemsNoAnom)/len(avgElemsNoAnom)
            except Exception as e:
                pass
        if len(avgElemsNoAnom) >= minCount:
            return sum(avgElemsNoAnom)/len(avgElemsNoAnom)

        if len(avgElemsAll) == 0:
            return -1
            
        return sum(avgElemsAll)/len(avgElemsAll)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--file", help="TSV file",type=str,required=True)
    parser.add_argument("-t","--threshold",help="Treshold",type=float)
    parser.add_argument("-m","--maxOccur",help="Max occurences in a row",type=int)
    parser.add_argument("-d","--delta",help="Temporal interval to segment the timeframe (seconds)",type=float)
    parser.add_argument("-c","--count",help="Minimum number of entries to calculate averages")
    args = parser.parse_args()
    if args.threshold:
        global threshold
        threshold = args.threshold
    if args.maxOccur:
        global maxOccur
        maxOccur = args.maxOccur
    if args.delta:
        global delta
        delta = args.delta
    if args.count:
        global minCount
        minCount = args.count

    keys = None
    values = []
    valuesTime = []
    startTime = 0
    count = 0
    avg = 0.0
    with open(args.file,'r') as filein:
        filein = csv.reader(filein,delimiter='\t')
        for row in filein:
            if keys == None:
                keys = dict()
                for val in row:
                    keys[val] = count
                    count+=1
            else:
                if startTime == 0:
                    startTime = float(row[keys['RTTMonitor.Timestamp']])
                    endtime = startTime - delta
                    avg = getAverage(args.file,endtime,keys)
                    if avg == -1:
                        return {'Anomaly':False,'Start':0,'LastStamp':0}

                if float(row[keys['RTTMonitor.Timestamp']]) < endtime:
                    break
                else:
                    values.append(float(row[keys['RTTMonitor.Min']]))
                    valuesTime.append(float(row[keys['RTTMonitor.Timestamp']]))
    values.reverse()
    valuesTime.reverse()
    anomCount = 0
    forCount = 0
    startTime = 0.0
    endTime = 0.0
    flagged = False
    for val in values:
        if val > avg * (1+threshold):
            anomCount+=1
            if anomCount >= maxOccur:
                flagged = True
                if startTime == 0.0:
                    startTime = valuesTime[forCount]
        else:
            anomCount = 0
            if startTime != 0.0:
                endTime = valuesTime[forCount]
        forCount+=1
    if startTime != 0.0:
        endTime = valuesTime[-1]
    return {'Anomaly':flagged,'Start':startTime,'LastStamp':endTime}


if __name__ == '__main__':
    print(main())
