# -*- coding: utf-8 -*-

"""
This module is the connectivity testing module.
It sends ICMP Request packets to the target(s), processess them slightly and sends them to the Proble Central Module.
Config File:
Time   -> time interval in seconds to repeat the ICMP requests
IPs    -> destination targets (list)
Number -> number of times to send ICMP requests per round
"""
import subprocess, platform
import argparse
import ipaddress
import datetime
import time
import json
import urllib3
import os
import sys
import numpy
cmd = ""
N = 2
server = ""
python_path = os.path.dirname(os.path.abspath(__file__))
"""
Uses the OS built-in ping tool to send the ICMP requests.
Could be implemented in pure Python, but that would force this program to be ran as root or administrator.
"""
def ping(destination_list):
	results = []
	for dst in destination_list:
		try:
			res = (subprocess.check_output(cmd + str(dst),shell=True,universal_newlines=True))
			sendToFlask(parser(res,dst))
		except subprocess.CalledProcessError:
			pass
	return results

"""
Parses the terminal output into a dict
-Destination
-Min
-Avg
-Max
-StdDev
-Percentile95
-Percentile50
-NumberRequests
-TimeStamp
"""

def parser(result,dst):
	values = []
	if result == "":
		return None
	else :
		for line in result.split('\n'):
			try:
				values.append(float(line.split('time=')[1][:-3]))
			except:
				pass
		if values == []:
			return None
		results = dict()
		results["Destination"] = str(dst)
		results["ModuleName"] = "RTTMonitor"
		results["Min"] = round(min(values),4)
		results["Avg"] = round(sum(values)/len(values),4)
		results["Max"] = round(max(values),4)
		results["StdDev"] = round(float(numpy.std(values)),4)
		percentile = numpy.percentile(values,[50,95])
		results["Percentile95"] = round(float(percentile[1]),4)
		results["Percentile50"] = round(float(percentile[0]),4)
		results["NumberRequests"] = len(values)
		results["Timestamp"] = str(time.time())
		return results
"""
Checks the received input parameters for the targets, checks if they're valid.
If they're valid, add them to the target list; if not, ignore.
"""
def getValidIP(ip_list):
	tmp = []
	for ip in ip_list:
		if not ip[0].isdigit():
			try:
				ip_list.append(socket.gethostbyname(ip))
			except:
				pass
		else:
			try:
				ipaddress.ip_address(ip)
				tmp.append(ip)
			except ValueError:
				pass
	return list(set(tmp))

"""
Sends to information to the Central Probe Module (Flask).
"""

def sendToFlask(results):
	print("send")
	print(results)
	enc_body = json.dumps(results)
	http = urllib3.PoolManager()
	req = http.request('POST',server+"/receiveData",headers={'Content-Type':'application/json'},body=enc_body)

def main():
	print("RTT Monitor")
	global N, cmd
	try:
		with open(python_path+"/monitor.json","r") as config_json:
			config = json.loads(config_json.read())
			destination_list = getValidIP(config["IPs"])
			if destination_list == []:
				print("Destination list is empty.")
				return
			N = config["Number"]
			wait = config["Time"]
	except:
		print("Configuration file not found.Exiting")
		return
	global server
	server = "localhost:40000"
	global cmd
	cmd = "ping -c " + str(N) + " "
	try:
		while True:
			ping(destination_list)
			time.sleep(wait)
	except Exception as ex:
		print(ex)
		pass
if __name__ == "__main__":
	main()

