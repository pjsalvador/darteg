import subprocess
import ipaddress
import time
import os
import re
from ipwhois.asn import IPASN
from ipwhois.net import Net
wait = 60
server = ""
def main():
    global wait
    try:
        with open(python_path+"/hoprttmonitor.json","r") as config_json:
            config = json.loads(config_json.read())
            destination_list = getValidIP(config["IPs"])
            if destination_list == []:
                print("Destination list is empty.")
                return
            wait = config["Time"]
    except:
        print("Configuration file not found.Exiting")
        return
    global server
    server = "localhost:40000"
    try:
        while True:
            for d in destination_list:
                try:
                    asns,rtts = trace(d)
                    timestamp = int(time.time())
                    asns,rtts = getData(asns,rtts)
                    results = wrap(asns,rtts,timestamp)
                    sendToFlask(results)
                except Exception as e:
                    pass
            time.sleep(wait)
    except Exception as ex:
        pass


def wrap(asns,rtts,timestamp):
    results = dict()
    results["Destination"] = str(dst)
    results["ModuleName"] = "HopRTTMonitor"
    results["ASNs"] = asns
    results["RTT"] = rtts
    results["Timestamp"] = timestamp
    return results


def getData(ASNs,RTTS):
    return str(ASNs)[1:-2],str(RTTS)[1:-2]

def trace(destination):
    ASNs = []
    RTTS = []
    try:
        res = (subprocess.check_output("sudo tcptraceroute " +str(destination) +" -n -q 10",shell=True,universal_newlines=True))
        ASNs,RTTS = parser(res)
    except subprocess.CalledProcessError:
        pass
    return ASNs,RTTS

def parser(result):
    ASNs = []
    RTTS = []
    if result == "":
        pass
    else :
        for i in result.split('\n'):
            try:
                tmp = re.findall("\d+.\d+.\d+.\d+",i)
                net = Net(tmp[0])
                ASN = int(IPASN(net).lookup()['asn'])
                if not ASN in ASNs:
                    ASNs.append(ASN)
                    rtts = re.findall("\d+.\d+ ms",i)
                    tmpRtt = []
                    for rtt in rtts:
                        tmpRtt.append(rtt.split(' ')[0])
                    rtt = min(list(map(float,tmpRtt)))
                    RTTS.append(rtt)
            except Exception as e:
                print(e)
                pass
    return ASNs,RTTS


"""
Checks the received input parameters for the targets, checks if they're valid.
If they're valid, add them to the target list; if not, ignore.
"""
def getValidIP(ip_list):
    tmp = []
    for ip in ip_list:
        if not ip[0].isdigit():
            try:
                ip_list.append(socket.gethostbyname(ip))
            except:
                pass
        else:
            try:
                ipaddress.ip_address(ip)
                tmp.append(ip)
            except ValueError:
                pass
    return list(set(tmp))

"""
Sends to information to the Central Probe Module (Flask).
"""

def sendToFlask(results):
    enc_body = json.dumps(results)
    http = urllib3.PoolManager()
    req = http.request('POST',server+"/receiveData",headers={'Content-Type':'application/json'},body=enc_body)


if __name__ == '__main__':
    main()
